package main

import (
	"log"
	"net"
	"time"
)

var (
	lastAddr *net.UDPAddr
)

func (o *options) client() {
	conn, err := net.DialUDP(o.network, nil, o.addr)
	if err != nil {
		log.Fatalln(err)
	}
	interval := time.Duration(o.clientOptions.interval) * time.Millisecond
	timeout := time.Duration(o.clientOptions.timeout) * time.Millisecond
	log.Println("interval:", interval, "timeout:", timeout)

	b := make([]byte, packetLength)
	var lastSent time.Time
	var msg []byte
	var p *packet
	for {
		if sleep := interval - time.Now().Sub(lastSent); sleep > 0 {
			time.Sleep(sleep)
		}

		msg, p = o.makeMsg()
		if msg != nil {
			_, err := conn.Write(o.seal(msg))
			if err != nil {
				log.Println("client send:", err)
				continue
			}
			lastSent = time.Now()
		}

		err := conn.SetReadDeadline(lastSent.Add(timeout))
		if err != nil {
			log.Fatalln("SetReadDeadline:", err)
		}
		for {
			n, err := conn.Read(b)
			if err != nil {
				if netErr, ok := err.(net.Error); ok {
					if netErr.Timeout() {
						o.log(p, true)
						break
					}
				}
				log.Println("client recv:", err)
				continue
			}
			if n != packetLength {
				continue
			}

			if msg := o.open(b[:n]); msg != nil {
				if p := o.parseMsg(msg); p != nil {
					o.log(p, false)
					if !addrEquals(lastAddr, p.Addr) {
						lastAddr = p.Addr
						log.Println("addr changed:", lastAddr)
						if o.url != "" {
							go o.updateDomains()
						}
					}
					break
				}
			}
		}
	}
}

func addrEquals(a *net.UDPAddr, b *net.UDPAddr) bool {
	return (a != nil && b != nil) && (a == b ||
		(a.Port == b.Port && a.IP.Equal(b.IP)))
}
