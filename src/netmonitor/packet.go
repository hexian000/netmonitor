package main

import (
	"bytes"
	"encoding/gob"
	"log"
	"net"
	"time"
)

const packetLength = 256

type packet struct {
	Seq  uint64
	Time int64
	Addr *net.UDPAddr
}

var seq = uint64(0)

func (o *options) makeMsg() ([]byte, *packet) {
	p := packet{
		Seq:  seq,
		Time: time.Now().UnixNano(),
		Addr: o.addr,
	}
	b, err := p.encode()
	if err != nil {
		log.Println("encode:", err)
		return nil, nil
	}
	seq++
	return b, &p
}

func (o *options) parseMsg(b []byte) *packet {
	var p packet
	if err := p.decode(b); err != nil {
		log.Println("decode:", err)
		return nil
	}
	return &p
}

func (p *packet) encode() ([]byte, error) {
	buf := &bytes.Buffer{}
	enc := gob.NewEncoder(buf)
	if err := enc.Encode(&p); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (p *packet) decode(b []byte) error {
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	return dec.Decode(p)
}
