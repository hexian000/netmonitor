package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var updateSemaphore = make(chan struct{}, 1)

func (o *options) updateDomains() {
	select {
	case updateSemaphore <- struct{}{}:
		defer func() {
			<-updateSemaphore
		}()
	default:
		log.Println("update domains in progress, skipping")
		return
	}

	client := &http.Client{
		Timeout: 1 * time.Minute,
	}
	var respCode int
	var respStr string
	retry(func() error {
		log.Println("Sending HTTP GET")
		resp, err := client.Get(o.url)
		if err != nil {
			return err
		}
		respCode = resp.StatusCode
		defer func() { _ = resp.Body.Close() }()
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		respStr = string(b)
		return nil
	})
	log.Println(respCode, respStr)
}

func retry(f func() error) {
	for err := f(); err != nil; err = f() {
		log.Println("http error:", err)
		time.Sleep(10 * time.Second)
	}
}
