package main

import (
	"log"
	"net"
	"time"
)

func (o *options) server() {
	conn, err := net.ListenUDP(o.network, o.addr)
	if err != nil {
		log.Fatalln(err)
	}
	rate := make(chan struct{}, 10)
	go func() {
		for range time.Tick(100 * time.Millisecond) {
			rate <- struct{}{}
		}
	}()
	b := make([]byte, packetLength)
	for range rate {
		n, remote, err := conn.ReadFromUDP(b)
		if err != nil {
			log.Println("server:", err)
			continue
		}
		if n != packetLength {
			continue
		}

		if msg := o.open(b[:n]); msg != nil {
			var p packet
			err := p.decode(msg)
			if err != nil {
				log.Println("decode:", err)
				continue
			}
			p.Addr = remote
			msg, err = p.encode()
			if err != nil {
				log.Println("encode:", err)
				continue
			}
			_, err = conn.WriteToUDP(o.seal(msg), remote)
			if err != nil {
				log.Println("server send:", err)
			}
		}
	}
}
