package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

func (o *options) open(b []byte) []byte {
	n := o.aead.NonceSize()
	if len(b) != packetLength {
		return nil
	}
	out, err := o.aead.Open([]byte{}, b[:n], b[n:], o.tagOpen)
	if err != nil {
		return nil
	}
	return out
}

func (o *options) seal(b []byte) []byte {
	n := o.aead.NonceSize()
	overhead := o.aead.Overhead()
	out := make([]byte, packetLength)
	nonce := out[:n]
	nonceGen.Fill(nonce)
	copy(out[n:], b)

	paddingStart := n + len(b)
	paddingEnd := packetLength - overhead
	if paddingEnd > paddingStart {
		_, _ = io.ReadFull(rand.Reader,
			out[paddingStart:paddingEnd])
	}

	plain := out[n : packetLength-overhead]
	o.aead.Seal(out[n:n], nonce, plain, o.tagSeal)
	return out
}

type nonceAES128 struct {
	seed  [aes.BlockSize]byte
	block cipher.Block
}

var nonceGen nonceAES128

func init() {
	var key [16]byte // aes-128
	_, _ = io.ReadFull(rand.Reader, key[:])
	_, _ = io.ReadFull(rand.Reader, nonceGen.seed[:])
	block, _ := aes.NewCipher(key[:])
	nonceGen.block = block
}

func (n *nonceAES128) Fill(nonce []byte) {
	for len(nonce) > 0 {
		if n.seed[0] == 0 { // entropy update
			_, _ = io.ReadFull(rand.Reader, n.seed[:])
		}
		n.block.Encrypt(n.seed[:], n.seed[:])
		done := copy(nonce, n.seed[:])
		nonce = nonce[done:]
	}
}
