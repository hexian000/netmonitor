package main

import (
	"fmt"
	"io"
	"log"
	"time"
)

func (o *options) log(p *packet, lost bool) {
	rtt := int64(-1)
	if !lost {
		rtt = (time.Now().UnixNano() - p.Time) / 1000000
	}

	if o.output != nil {
		_, _ = io.WriteString(o.output, fmt.Sprintf(
			"\"%v\"\t%d\t%d\n",
			time.Unix(0, p.Time).Format(time.RFC3339),
			p.Seq,
			rtt,
		))
	} else {
		if lost {
			log.Println(
				"seq:", p.Seq,
				"timeout",
			)
		} else {
			log.Println(
				"addr:", p.Addr.String(),
				"seq:", p.Seq,
				"RTT:", rtt,
			)
		}
	}
}
