package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/json"
	"flag"
	"golang.org/x/crypto/pbkdf2"
	"io/ioutil"
	"log"
	"net"
	"net/url"
	"os"
)

const (
	salt      = "netmonitor"
	tagServer = "pong"
	tagClient = "ping"
)

type Config struct {
	Address    string
	Key        string
	Interval   int
	Timeout    int
	Output     string
	ServerMode bool
	Net        string

	// DuckDNS API
	Domains string
	Token   string
}

type options struct {
	network string
	addr    *net.UDPAddr
	aead    cipher.AEAD
	tagOpen []byte
	tagSeal []byte

	*clientOptions
}

type clientOptions struct {
	interval int
	timeout  int
	output   *RotateWriter
	url      string
}

func prepare() (*options, bool) {
	var flagHelp bool
	var flagConfigFile string
	flag.BoolVar(&flagHelp, "h", false, "show usage")
	flag.StringVar(&flagConfigFile, "c", "", "use config file")
	config := Config{
		Address:    ":7333",
		Interval:   1000,
		Timeout:    4000,
		ServerMode: false,
		Net:        "udp",
	}
	flag.StringVar(&config.Address, "a", config.Address, "listen address")
	flag.StringVar(&config.Key, "k", config.Key, "key")
	flag.IntVar(&config.Interval, "i", config.Interval, "interval in ms")
	flag.IntVar(&config.Timeout, "w", config.Timeout, "timeout in ms")
	flag.StringVar(&config.Output, "o", config.Output, "output file in tsv format")
	flag.BoolVar(&config.ServerMode, "s", config.ServerMode, "run server")
	flag.StringVar(&config.Net, "n", config.Net, "specify network")
	flag.Parse()
	if flagHelp {
		flag.Usage()
		os.Exit(0)
	}
	if flagConfigFile != "" {
		b, err := ioutil.ReadFile(flagConfigFile)
		if err != nil {
			log.Fatalln("read config:", err)
		}
		err = json.Unmarshal(b, &config)
		if err != nil {
			log.Fatalln("parse config:", err)
		}
	}
	if config.Key == "" {
		log.Fatalln("Key is required.")
	}
	if config.Interval < 1000 || config.Timeout < 1000 {
		log.Fatalln("Huh?")
	}

	addr, err := net.ResolveUDPAddr(config.Net, config.Address)
	if err != nil {
		log.Fatalln(err)
	}

	k := pbkdf2.Key([]byte(config.Key), []byte(salt), 4096, 32, sha256.New)
	block, err := aes.NewCipher(k)
	if err != nil {
		log.Fatalln("crypto:", err)
	}
	aead, err := cipher.NewGCM(block)
	if err != nil {
		log.Fatalln("crypto:", err)
	}

	var outFile *RotateWriter
	if config.Output != "" {
		outFile = CreateRotateWriteCloser(config.Output, nil)
	}

	o := &options{
		network: config.Net,
		addr:    addr,
		aead:    aead,
	}

	if config.ServerMode {
		o.tagOpen = []byte(tagClient)
		o.tagSeal = []byte(tagServer)
	} else {
		o.tagOpen = []byte(tagServer)
		o.tagSeal = []byte(tagClient)
		o.clientOptions = &clientOptions{
			interval: config.Interval,
			timeout:  config.Timeout,
			output:   outFile,
		}
		if config.Domains != "" && config.Token != "" {
			u, err := url.Parse("https://www.duckdns.org/update")
			if err != nil {
				log.Fatal(err)
			}
			q := u.Query()
			q.Set("domains", config.Domains)
			q.Set("token", config.Token)
			u.RawQuery = q.Encode()
			o.url = u.String()
		}
	}

	// discard sensitive info explicitly
	config.Key = ""

	return o, config.ServerMode
}

func main() {
	if o, mode := prepare(); mode {
		log.Println("starting server")
		o.server()
	} else {
		log.Println("starting client")
		o.client()
	}
}
